'use strict';

app
    .service('BookService', function($http) {
        console.log('BookService');

        this.search = function (book, okCb, errCb) {
            console.log("Search for: " + JSON.stringify(book));
            $http.get('/book?author=' + book.author + "&title="  + book.title + "&genre=" + book.genre).then(okCb, errCb);
        };

        this.orderBook = function (order, okCb, errCb) {
            console.log("Order: " + JSON.stringify(order));
             $http.post('/book/order', order, {}).then(okCb, errCb);
        };
        this.returnBook = function (order, okCb, errCb) {
            console.log("Return: " + JSON.stringify(order));
             $http.post('/book/return', order, {}).then(okCb, errCb);
        };

       this.finaliseOrder = function (order, okCb, errCb) {
           console.log("Finalise " + JSON.stringify(order));
           $http.get('/book/finalise?userId='+order.userId).then(okCb, errCb);
       };

       this.getTotal = function (order, okCb, errCb) {
           console.log("GetTotal for: " + JSON.stringify(order));
           $http.get('/book/total?userId='+order.userId).then(okCb, errCb);
       };

       this.getCart = function (order, okCb, errCb) {
           console.log("GetCart: " + JSON.stringify(order));
           $http.get('/book/cart?userId='+order.userId).then(okCb, errCb);
       };

       this.getBorrow = function (order, okCb, errCb) {
           console.log("Borrow for: " + JSON.stringify(order));
           $http.get('/book/borrowed?userId='+order.userId).then(okCb, errCb);
       };

       this.getBuy = function (order, okCb, errCb) {
           console.log("Buy for: " + JSON.stringify(order));
           $http.get('/book/bought?userId='+order.userId).then(okCb, errCb);
       };
      this.resetCart = function(order, okCb, errCb) {
          console.log("ResetCart for: " + JSON.stringify(order));
          $http.get('/book/reset?userId='+order.userId).then(okCb, errCb);
      };
    });