'use strict';

app
    .service('UserService', function($http) {
        console.log('UserService');

        this.login = function (user, okCb, errCb) {
            console.log("Login: " + JSON.stringify(user));
            $http.post('/user/login', user, {}).then(okCb, errCb);
        };

        this.register = function (user, okCb, errCb) {
            console.log("Register: " + JSON.stringify(user))
            $http.post('/user/register', user, {}).then(okCb, errCb);
        };

        this.getFunds = function (user, okCb, errCb) {
            console.log("Funds: " + JSON.stringify(user))
            $http.get('/user/funds?userId=' + user.userId, {}).then(okCb, errCb);
        };
        this.logout = function (user, okCb, errCb) {
            console.log("LogOut: " + JSON.stringify(user))
            $http.get('/user/logout?userId=' + user.userId, {}).then(okCb, errCb);
        };
    });