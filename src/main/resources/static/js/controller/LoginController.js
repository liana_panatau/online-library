'use strict';

app
    .controller('LoginController', ['$scope', '$location', 'activeUser', 'UserService', 'FlashService', function($scope, $location, activeUser, UserService, FlashService) {
        console.log('LoginController');
        $("#alert").hide();

        $scope.login = function(user) {
            UserService.login($scope.user, function(response) {
                $("#alert").hide();

                console.log("Response " + JSON.stringify(response.data));
                activeUser.userId=response.data.userId;
                $location.path('/home');
            }, function(error) {
                console.log('Login error ' + JSON.stringify(error));
                $("#alert").show();
                FlashService.Error('Invalid username/password', false);

                $location.path('/');
            });
        };
    }]);