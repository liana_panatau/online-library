'use strict';

app
    .controller('HomeController', ['$scope', '$location', 'BookService','UserService', 'activeUser', function($scope, $location, BookService, UserService, activeUser) {
        console.log('HomeController ' + activeUser.userId);
        $('#myAlert').hide();
        $('#alertOrder').hide();
        $('#alertFunds').hide();
        $('#alertSucc').hide();

        updateCart();

        $scope.total = 0;
        function updateCart() {
            var order = {'userId':activeUser.userId};
            BookService.getCart(order, function(response) {
                $scope.cartBooks = [];

                response.data.forEach(function(entry) {
                    $scope.cartBooks.push({
                                        'quantity': entry.quantity,
                                        'name': entry.name,
                                        'price':entry.price,
                                        'title':entry.title
                    });
                });
            }, function(error) {
                console.log("Error getting cart!" + error);
            });

            BookService.getTotal(order, function(response) {
                 $scope.total = response.data.total;
            }, function(error) {
                console.log("Error getting cart!");
            });
        };

        function reset() {
            $scope.author = false;
            $scope.title = false;
            $scope.genre = false;
            $scope.query = "";
        };

        $scope.submit = function() {
            console.log("Author=" + $scope.author + "|Title=" + $scope.title + "|Genre=" + $scope.genre);

            var numberNotChecked = $('input:checkbox:not(":checked")').length;
            if (numberNotChecked != 2) {
                $('#myAlert').show();
                reset();
            } else {
                $('#myAlert').hide();
                var book = {};
                if ($scope.author) { book.author = $scope.query; }
                if ($scope.title) { book.title = $scope.query; }
                if ($scope.genre) { book.genre = $scope.query; }

                BookService.search(book, function(response) {
                    console.log("Response  " + JSON.stringify(response.data));
                    $scope.books = [];

                    response.data.forEach(function(entry) {
                        $scope.books.push({
                                            'catalogid': entry.catalogid,
                                            'author': entry.author,
                                            'title':entry.title,
                                            'pages':entry.pages,
                                            'publication':entry.publication,
                                            'release_year':entry.release_year,
                                            'quantity':entry.quantity,
                                            'genre':entry.genre,
                                            'price':entry.price
                        });
                    });
                    reset();
                }, function(error) {
                    console.log('Error ' + JSON.stringify(error));
                });
            }
        };
        $scope.goAccount = function() {
                $location.path('/account');
        };

        $scope.buy = function(catalogid) {
            console.log("Buy " + catalogid);
            var order = {'catalogId': catalogid, 'userId':activeUser.userId, 'orderType':'buy'};
            BookService.orderBook(order, function(response) {
                console.log('Buy response' + JSON.stringify(response));
                updateCart();
            }, function(error) {
                $("#alertOrder").fadeTo(2000, 500).slideUp(500, function(){
                    $("#alertOrder").slideUp(500);
                });
                console.log('Buy error' + JSON.stringify(error));
            });
        };

        $scope.borrow = function(catalogid) {
            console.log("Borrow" + catalogid);
            var order = {'catalogId': catalogid, 'userId':activeUser.userId, 'orderType':'borrow'};
            BookService.orderBook(order, function(response) {
                console.log('Borrow response' + JSON.stringify(response));
                updateCart();
            }, function(error) {
                $("#alertOrder").fadeTo(2000, 500).slideUp(500, function(){
                    $("#alertOrder").slideUp(500);
                });
                console.log('Borrow error' + JSON.stringify(error));
            });
        };

        $scope.finalize = function() {
            console.log("Finalise order!");
            var order = {'userId':activeUser.userId};
            BookService.finaliseOrder(order, function(response) {
                console.log('Response ' + JSON.stringify(response));
                $("#alertSucc").fadeTo(2000, 500).slideUp(500, function(){
                    $("#alertSucc").slideUp(500);
                });
                updateCart();
            }, function(error) {
                $("#alertFunds").fadeTo(2000, 500).slideUp(500, function(){
                    $("#alertFunds").slideUp(500);
                });
                console.log('Error' + JSON.stringify(error));
            });};

        $scope.resetCart = function() {
            console.log("ResetCart");
            var order = {'userId':activeUser.userId};

            BookService.resetCart(order, function(response) {
                console.log('Response ' + JSON.stringify(response));
                updateCart();
            }, function(error) {
                console.log('Error' + JSON.stringify(error));
        });};

        $scope.logout = function() {
           console.log('logout')
           var order = {'userId':activeUser.userId};

           UserService.logout(order, function(response) {
               console.log('Response ' + JSON.stringify(response));
               $location.path('/');
           }, function(error) {
               console.log('Error' + JSON.stringify(error));
           });
        };
    }]);