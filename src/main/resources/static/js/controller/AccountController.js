'use strict';

app
    .controller('AccountController', ['$scope', '$location', 'activeUser', 'BookService', 'UserService', 'FlashService', function($scope, $location, activeUser, BookService, UserService, FlashService) {
        console.log('AccountController ' + activeUser.userId);



        $scope.funds = 0;
        var user = {'userId':activeUser.userId};

        function init(){
            $scope.boughtBooks = [];
            $scope.borrowBooks = [];

            UserService.getFunds(user, function(response) {
                $scope.funds = response.data.funds;
            }, function(error) {
                console.log('Error ' + JSON.stringify(error));
            });

            var order = {'userId':activeUser.userId};
            BookService.getBorrow(order, function(response) {
                console.log("Response  " + JSON.stringify(response.data));
                response.data.forEach(function(entry) {
                    $scope.borrowBooks.push({
                                        'catalogid': entry.catalogid,
                                        'author': entry.author,
                                        'title':entry.title,
                                        'pages':entry.pages,
                                        'publication':entry.publication,
                                        'release_year':entry.release_year,
                                        'quantity':entry.quantity,
                                        'genre':entry.genre,
                                        'endtime':entry.returnDate
                    });
                });
            }, function(error) {
                console.log('Error ' + JSON.stringify(error));
            });
            BookService.getBuy(order, function(response) {
                console.log("Response  " + JSON.stringify(response.data));
                response.data.forEach(function(entry) {
                    $scope.boughtBooks.push({
                                        'catalogid': entry.catalogid,
                                        'author': entry.author,
                                        'title':entry.title,
                                        'pages':entry.pages,
                                        'publication':entry.publication,
                                        'release_year':entry.release_year,
                                        'quantity':entry.quantity,
                                        'genre':entry.genre
                    });
                });
            }, function(error) {
                console.log('Error ' + JSON.stringify(error));
            });
        };

        init();
        $scope.removeBook = function(catalogid) {
            console.log("Remove " + catalogid);
            var order = {'catalogId': catalogid, 'userId':activeUser.userId, 'orderType':'buy'};
            BookService.returnBook(order, function(response) {
                console.log('Response ' + JSON.stringify(response));
                init();
            }, function(error) {
            });};

        $scope.returnBook = function(catalogid) {
            console.log("Return " + catalogid);
            var order = {'catalogId': catalogid, 'userId':activeUser.userId, 'orderType':'borrow'};
            BookService.returnBook(order, function(response) {
                console.log('Response ' + JSON.stringify(response));
                init();
            }, function(error) {
                console.log('Error' + JSON.stringify(error));
            });
         };

        $scope.goHome = function() {
            $location.path('/home');
         };
    }]);