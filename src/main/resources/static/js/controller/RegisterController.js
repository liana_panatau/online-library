'use strict';

app
    .controller('RegisterController', ['$scope', '$location', 'activeUser','UserService', 'FlashService', function($scope, $location, activeUser, UserService, FlashService) {
        console.log('RegisterController');

        $scope.register = function(user) {
            UserService.register($scope.user, function(response) {
                console.log("Response " + JSON.stringify(response.data));

                activeUser.userId = response.data.userId;
                FlashService.Success('Succesful registration', false);
                $location.path('/home');
            }, function(error) {
                console.log('Error ' + JSON.stringify(error));
                alert(error.data.message);
            });
        };
    }]);