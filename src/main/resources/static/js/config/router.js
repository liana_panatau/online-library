app
    .config(function ($routeProvider) {
        console.log("Routing config.");

        $routeProvider
            .when('/home', {
                templateUrl: 'template/home.html',
                controller: 'HomeController'
            })
            .when('/', {
                templateUrl: 'template/login.html',
                controller: 'LoginController'
            })
            .when('/register', {
                templateUrl: 'template/register.html',
                controller: 'RegisterController'
            })
            .when('/account', {
                templateUrl: 'template/account.html',
                controller: 'AccountController'
            });
    });