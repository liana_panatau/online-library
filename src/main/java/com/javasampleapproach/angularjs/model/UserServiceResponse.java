package com.javasampleapproach.angularjs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserServiceResponse {
    private String message;
    private Integer userId;
    private Integer funds;

    public UserServiceResponse() {
    }

    public UserServiceResponse(String message, Integer userId) {
        this.message = message;
        this.userId = userId;
    }


    public UserServiceResponse(Integer funds) {
        this.funds = funds;
    }

    public Integer getFunds() {
        return funds;
    }

    public void setFunds(Integer funds) {
        this.funds = funds;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "UserServiceResponse{" +
                "message='" + message + '\'' +
                ", userId=" + userId +
                '}';
    }
}
