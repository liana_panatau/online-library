package com.javasampleapproach.angularjs.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {
    private Integer catalogId;
    private Integer userId;
    private String orderType;

    public Order() {
    }

    public Order(Integer catalogId, Integer userId, String orderType) {
        this.catalogId = catalogId;
        this.userId = userId;
        this.orderType = orderType;
    }

    public Integer getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Integer catalogId) {
        this.catalogId = catalogId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @Override
    public String toString() {
        return "Order{" +
                "catalogId=" + catalogId +
                ", userId=" + userId +
                ", orderType='" + orderType + '\'' +
                '}';
    }
}
