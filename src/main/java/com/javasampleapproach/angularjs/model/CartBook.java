package com.javasampleapproach.angularjs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartBook
{
    private Integer quantity;
    private String title;
    private String name;
    private Integer price;

    public CartBook(Integer quantity, String title, String name, Integer price) {
        this.quantity = quantity;
        this.title = title;
        this.name = name;
        this.price = price;
    }

    public CartBook() {
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CartBook cartBook = (CartBook) o;

        if (quantity != null ? !quantity.equals(cartBook.quantity) : cartBook.quantity != null) return false;
        if (title != null ? !title.equals(cartBook.title) : cartBook.title != null) return false;
        if (name != null ? !name.equals(cartBook.name) : cartBook.name != null) return false;
        if (price != null ? !price.equals(cartBook.price) : cartBook.price != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = quantity != null ? quantity.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "CartBook{" +
                "quantity=" + quantity +
                ", title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
