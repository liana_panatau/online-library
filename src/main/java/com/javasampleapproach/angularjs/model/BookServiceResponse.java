package com.javasampleapproach.angularjs.model;

import java.util.List;

public class BookServiceResponse {
    private String message;
    private Integer total;

    public BookServiceResponse() {
    }

    public BookServiceResponse(String message) {
        this.message = message;
    }


    public BookServiceResponse(Integer total) {
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookServiceResponse that = (BookServiceResponse) o;

        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        return total != null ? total.equals(that.total) : that.total == null;

    }

    @Override
    public int hashCode() {
        int result = message != null ? message.hashCode() : 0;
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BookServiceResponse{" +
                "message='" + message + '\'' +
                ", total=" + total +
                '}';
    }
}
