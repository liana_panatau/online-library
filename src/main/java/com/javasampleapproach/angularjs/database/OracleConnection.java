package com.javasampleapproach.angularjs.database;

import com.javasampleapproach.angularjs.exeption.BookException;
import com.javasampleapproach.angularjs.exeption.UserException;
import com.javasampleapproach.angularjs.model.Book;
import com.javasampleapproach.angularjs.model.CartBook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static oracle.jdbc.OracleTypes.CURSOR;
import static oracle.jdbc.OracleTypes.NUMBER;


@Component
public class OracleConnection {

    private Connection connection;

    @Value("${connection.username}")
    private String username;
    @Value("${connection.password}")
    private String password;
    @Value("${connection.host}")
    private String host;
    @Value("${connection.port}")
    private int port;
    @Value("${connection.schema}")
    private String schema;

    public OracleConnection() {

    }

    public OracleConnection(Connection connection, String username, String password, String host, int port, String schema) {
        this.connection = connection;
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        this.schema = schema;
    }
    public void openConnection() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            System.out.println("JDBC check: PASSED!");
        } catch (ClassNotFoundException CNFEx) {
            System.out.println("JDBC check: NOT PASSED!");
            CNFEx.printStackTrace();
        }

        try {
            String connString = "jdbc:oracle:thin:@" + this.host + ":" +
                    this.port + ":" + this.schema;
            this.connection = DriverManager.getConnection(connString,
                    this.username, this.password);
            if (this.connection != null) {
                System.out.println("We are connected!");
            } else {
                System.out.println("Failed to make a connection!");
            }
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            this.connection.close();
            System.out.println("Connection closed!");
        } catch (Exception e) {
            System.out.println("Connection NOT closed!");
            e.printStackTrace();
        }
    }

    public Integer login(String username, String password, String token) {
        String call = "{? = call user_package.login(?, ?, ?)}";
        Integer userId;

        try {
            CallableStatement callableStatement = this.connection.prepareCall(call);
            callableStatement.registerOutParameter(1, NUMBER);
            callableStatement.setString(2, username);
            callableStatement.setString(3, password);
            callableStatement.setString(4, token);

            callableStatement.executeQuery();

            userId = callableStatement.getInt(1);
        } catch (SQLException e) {
            System.out.println("Error user login " + username);
            throw new UserException(getError(e));
        }

        return userId;
    }

    public Integer register(String username, String password, String firstname, String lastname, String token) {
        String call = "{? = call user_package.signin(?, ?, ?, ?, ?)}";
        Integer userId;

        try {
            CallableStatement callableStatement = this.connection.prepareCall(call);
            callableStatement.registerOutParameter(1, NUMBER);
            callableStatement.setString(2, firstname);
            callableStatement.setString(3, lastname);
            callableStatement.setString(4, username);
            callableStatement.setString(5, password);
            callableStatement.setString(6, "a");

            callableStatement.executeQuery();

            userId = callableStatement.getInt(1);
        } catch (SQLException e) {
            System.out.println("Error user register " + username);
            throw new UserException(getError(e));
        }

        return userId;
    }

    public void logout(Integer userId, String token) {
        String call = "{call user_package.logout(?, ?)}";
        try {
            CallableStatement callableStatement = this.connection.prepareCall(call);
            callableStatement.setInt(1, userId);
            callableStatement.setString(2, token);

            callableStatement.executeQuery();
        } catch (SQLException e) {
            System.out.println("Error user logout " + username);
            throw new UserException(getError(e));
        }
    }

    private String getError(SQLException e) {
        return Optional.of(e.getLocalizedMessage().substring(e.getLocalizedMessage().indexOf(":") + 1, e.getLocalizedMessage().indexOf("\n"))).orElse(e.getMessage());
    }


    public List<Book> searchByAuthor(String author) {
        List<Book> books = new ArrayList<>();
        String firstname;
        String lastname;

        String[] authorname = author.split(" ");
        if (authorname.length == 2) {
            firstname = authorname[0];
            lastname = authorname[1];
        } else {
            firstname = authorname[0];
            lastname = null;
        }


        String call = "{call search_books_package.by_author(?, ?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setString("v_firstname", firstname);
            statement.setString("v_lastname", lastname);
            statement.registerOutParameter("books", CURSOR);

            statement.executeQuery();

            ResultSet rs = (ResultSet) statement.getObject("books");
            while(rs.next()){
                Integer catalogid = rs.getInt("catalog_id");
                String authorFirstname = rs.getString("firstname");
                String authorLastname = rs.getString("lastname");
                String title = rs.getString("title");
                Integer pages = rs.getInt("pages");
                String genre = rs.getString("genre");
                String publication = rs.getString("publication");
                Date release_year = rs.getDate("release_year");
                Integer price = rs.getInt("price");
                Integer quantity = rs.getInt("quantity");


                books.add(new Book(catalogid, authorFirstname + " " + authorLastname, title, pages, publication, release_year, quantity, genre, price, null));
            }
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }

        return books;
    }

    public List<Book> searchByTitle(String title) {
        List<Book> books = new ArrayList<>();

        String call = "{call search_books_package.by_title(?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setString("v_title", title);
            statement.registerOutParameter("books", CURSOR);

            statement.executeQuery();

            ResultSet rs = (ResultSet) statement.getObject("books");
            while(rs.next()){
                Integer catalogid = rs.getInt("catalog_id");
                String authorFirstname = rs.getString("firstname");
                String authorLastname = rs.getString("lastname");
                String title1 = rs.getString("title");
                Integer pages = rs.getInt("pages");
                String genre = rs.getString("genre");
                String publication = rs.getString("publication");
                Date release_year = rs.getDate("release_year");
                Integer price = rs.getInt("price");
                Integer quantity = rs.getInt("quantity");


                books.add(new Book(catalogid, authorFirstname + " " + authorLastname, title1, pages, publication, release_year, quantity, genre, price, null));
            }
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }

        return books;
    }

    public List<Book> searchByGenre(String genre) {
        List<Book> books = new ArrayList<>();

        String call = "{call search_books_package.by_genre(?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setString("v_genre", genre);
            statement.registerOutParameter("books", CURSOR);

            statement.executeQuery();

            ResultSet rs = (ResultSet) statement.getObject("books");
            while(rs.next()){
                Integer catalogid = rs.getInt("catalog_id");
                String authorFirstname = rs.getString("firstname");
                String authorLastname = rs.getString("lastname");
                String title = rs.getString("title");
                Integer pages = rs.getInt("pages");
                String genre1 = rs.getString("genre");
                String publication = rs.getString("publication");
                Date release_year = rs.getDate("release_year");
                Integer price = rs.getInt("price");
                Integer quantity = rs.getInt("quantity");


                books.add(new Book(catalogid, authorFirstname + " " + authorLastname, title, pages, publication, release_year, quantity, genre1, price,  null));
            }
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }

        return books;
    }


    public void orderBook(Integer catalogId, Integer userId, String orderType) {
        String call = "{call order_package.order_book(?, ?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setInt(1, catalogId);
            statement.setInt(2, userId);
            statement.setString(3, orderType);

            statement.executeQuery();
        } catch (SQLException e) {
            System.out.println(e);
            throw new BookException(getError(e));
        }
    }

    public List<CartBook> getCart(Integer userId) {
        List<CartBook> cartBooks = new ArrayList<>();

        String call = "{call order_package.get_cart(?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setInt("v_userid", userId);
            statement.registerOutParameter("cart_books", CURSOR);

            statement.executeQuery();
            ResultSet rs = (ResultSet) statement.getObject("cart_books");
            while (rs.next()) {
                cartBooks.add(new CartBook(rs.getInt("quantity"), rs.getString("title"), rs.getString("name"), rs.getInt("price")));
            }

        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }

        return cartBooks;
    }

    public Integer getCartTotalPrice(Integer userId) {
        String call = "{? = call order_package.get_total_price(?)}";
        Integer totalPrice = 0;
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.registerOutParameter(1, NUMBER);
            statement.setInt(2, userId);

            statement.executeQuery();
            totalPrice = statement.getInt(1);
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }

        return totalPrice;
    }
    public void finalise(Integer userId, Integer totalPrice) {
        String call = "{call order_package.finalise(?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setInt(1, userId);
            statement.setInt(2, totalPrice);

            statement.executeQuery();
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }
    }

    public void returnBook(Integer catalogId, Integer userId, String bookType) {
        String call = "{call order_package.return_book(?, ?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setInt(1, catalogId);
            statement.setInt(2, userId);
            statement.setString(3, bookType);

            statement.executeQuery();
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }
    }

    public List<Book> getUserBooks(Integer userId, String bookType) {
        List<Book> books = new ArrayList<>();

        String call = "{call user_card_package.get_user_books(?, ?, ?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setInt("v_userid", userId);
            statement.setString("v_booktype", bookType);
            statement.registerOutParameter("user_books", CURSOR);

            statement.executeQuery();

            ResultSet rs = (ResultSet) statement.getObject("user_books");
            while(rs.next()){
                Integer catalogid = rs.getInt("catalog_id");
                String name = rs.getString("name");
                String title = rs.getString("title");
                Integer pages = rs.getInt("pages");
                String genre = rs.getString("genre");
                String publication = rs.getString("publication");
                Date release_year = rs.getDate("release_year");

                Date returnDate = null;
                if (bookType.equals("borrow")) {
                    returnDate  = rs.getDate("return_date");
                }
                books.add(new Book(catalogid, name, title, pages, publication, release_year, 1, genre, null,  returnDate));
            }
        } catch (SQLException e) {
            System.out.println(e);
            throw new BookException(e.getMessage());
        }

        return books;
    }

    public Integer getFunds(Integer userId) {
        Integer totalFonds;
        String call = "{? = call user_package.get_funds(?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.registerOutParameter(1, NUMBER);
            statement.setInt(2, userId);

            statement.executeQuery();
            totalFonds = statement.getInt(1);
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }

        return totalFonds;
    }

    public void resetCart(Integer userId) {
        String call = "{call order_package.resetCart(?)}";
        try {
            CallableStatement statement = this.connection.prepareCall(call);
            statement.setInt(1, userId);

            statement.executeQuery();
        } catch (SQLException e) {
            throw new BookException(e.getMessage());
        }

    }
}
