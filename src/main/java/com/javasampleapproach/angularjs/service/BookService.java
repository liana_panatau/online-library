package com.javasampleapproach.angularjs.service;

import com.javasampleapproach.angularjs.model.Book;
import com.javasampleapproach.angularjs.model.BookSearchRequest;
import com.javasampleapproach.angularjs.model.CartBook;
import com.javasampleapproach.angularjs.model.Order;

import java.util.List;

public interface BookService {
    List<Book> search(BookSearchRequest bookSearchRequest);
    void orderBook(Order order);
    void returnBook(Order order);
    void finaliseOrder(Integer userId, Integer totalPrice);
    Integer getTotal(Integer userId);
    List<CartBook> getCart(Integer userId);
    List<Book> getBorrowedBooks(Integer userId);
    List<Book> getBoughtBooks(Integer userId);
    void resetCart(Integer userId);
}
