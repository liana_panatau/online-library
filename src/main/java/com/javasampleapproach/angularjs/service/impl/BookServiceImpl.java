package com.javasampleapproach.angularjs.service.impl;

import com.javasampleapproach.angularjs.database.OracleConnection;
import com.javasampleapproach.angularjs.model.Book;
import com.javasampleapproach.angularjs.model.BookSearchRequest;
import com.javasampleapproach.angularjs.model.CartBook;
import com.javasampleapproach.angularjs.model.Order;
import com.javasampleapproach.angularjs.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private OracleConnection oracleConnection;

    @Override
    public List<Book> search(BookSearchRequest bookSearchRequest) {
        oracleConnection.openConnection();
        List<Book> books = new ArrayList<>();

        String author = bookSearchRequest.getAuthor();
        String genre = bookSearchRequest.getGenre();
        String title = bookSearchRequest.getTitle();

        if (!StringUtils.isEmpty(author) && !author.equals("undefined")) {
            books = oracleConnection.searchByAuthor(author);
        } else if (!StringUtils.isEmpty(genre) && !genre.equals("undefined")){
            books = oracleConnection.searchByGenre(genre);
        } else if (!StringUtils.isEmpty(title) && !title.equals("undefined")) {
            books = oracleConnection.searchByTitle(title);
        }
        oracleConnection.closeConnection();

        return books;
    }

    @Override
    public void returnBook(Order order) {
        oracleConnection.openConnection();
        oracleConnection.returnBook(order.getCatalogId(), order.getUserId(), order.getOrderType());
        oracleConnection.closeConnection();
    }

    @Override
    public void orderBook(Order order) {
        oracleConnection.openConnection();
        oracleConnection.orderBook(order.getCatalogId(), order.getUserId(), order.getOrderType());
        oracleConnection.closeConnection();
    }

    @Override
    public void finaliseOrder(Integer userId, Integer totalPrice) {
        oracleConnection.openConnection();
        oracleConnection.finalise(userId, totalPrice);
        oracleConnection.closeConnection();
    }

    @Override
    public Integer getTotal(Integer userId) {
        oracleConnection.openConnection();
        Integer cartTotalPrice = oracleConnection.getCartTotalPrice(userId);
        oracleConnection.closeConnection();

        return cartTotalPrice;
    }

    @Override
    public List<CartBook> getCart(Integer userId) {
        oracleConnection.openConnection();
        List<CartBook> cart = oracleConnection.getCart(userId);
        oracleConnection.closeConnection();

        return cart;
    }

    @Override
    public List<Book> getBorrowedBooks(Integer userId) {
        oracleConnection.openConnection();
        List<Book> books = oracleConnection.getUserBooks(userId, "borrow");
        oracleConnection.closeConnection();

        return books;
    }

    @Override
    public List<Book> getBoughtBooks(Integer userId) {
        oracleConnection.openConnection();
        List<Book> books = oracleConnection.getUserBooks(userId, "buy");
        oracleConnection.closeConnection();

        return books;
    }

    @Override
    public void resetCart(Integer userId) {
        oracleConnection.openConnection();
        oracleConnection.resetCart(userId);
        oracleConnection.closeConnection();
    }
}
