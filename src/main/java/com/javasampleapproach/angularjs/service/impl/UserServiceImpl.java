package com.javasampleapproach.angularjs.service.impl;

import com.javasampleapproach.angularjs.database.OracleConnection;
import com.javasampleapproach.angularjs.model.User;
import com.javasampleapproach.angularjs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private OracleConnection oracleConnection;

    @Override
    public Integer loginUser(User user) {
        oracleConnection.openConnection();
        Integer userId = oracleConnection.login(user.getUsername(), user.getPassword(), null);
        oracleConnection.closeConnection();

        return userId;
    }

    @Override
    public Integer registerUser(User user) {
        oracleConnection.openConnection();
        Integer userId = oracleConnection.register(user.getUsername(),  user.getPassword(), user.getFirstname(), user.getLastname(), null);
        oracleConnection.closeConnection();

        return userId;
    }

    @Override
    public void logoutUser(Integer userId) {
        oracleConnection.openConnection();
        oracleConnection.logout(userId,  null);
        oracleConnection.closeConnection();
    }

    @Override
    public Integer getFunds(Integer userId) {
        Integer funds;

        oracleConnection.openConnection();
        funds = oracleConnection.getFunds(userId);
        oracleConnection.closeConnection();

        return funds;
    }
}
