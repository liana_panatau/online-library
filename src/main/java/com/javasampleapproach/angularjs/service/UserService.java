package com.javasampleapproach.angularjs.service;

import com.javasampleapproach.angularjs.model.User;

public interface UserService {
    Integer loginUser(User user);
    Integer registerUser(User user);
    void logoutUser(Integer userId);
    Integer getFunds(Integer userId);
}
