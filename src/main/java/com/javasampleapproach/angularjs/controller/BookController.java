package com.javasampleapproach.angularjs.controller;

import com.javasampleapproach.angularjs.exeption.BookException;
import com.javasampleapproach.angularjs.model.*;
import com.javasampleapproach.angularjs.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Book>> searchBook(@RequestParam String author, @RequestParam String title, @RequestParam String genre){
        List<Book> books = bookService.search(new BookSearchRequest(author, title, genre));
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public ResponseEntity<BookServiceResponse> orderBook(@RequestBody Order order){
        try {
            bookService.orderBook(order);
        } catch (BookException ex) {
            return new ResponseEntity<>(new BookServiceResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BookServiceResponse("Order success"), HttpStatus.OK);
    }


    @RequestMapping(value = "/return", method = RequestMethod.POST)
    public ResponseEntity<BookServiceResponse> returnBook(@RequestBody Order order){
        try {
            bookService.returnBook(order);
        } catch (BookException ex) {
            return new ResponseEntity<>(new BookServiceResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BookServiceResponse("Success"), HttpStatus.OK);
    }

    @RequestMapping(value = "/finalise", method = RequestMethod.GET)
    public ResponseEntity<BookServiceResponse> finalise(@RequestParam Integer userId){
        try {
            bookService.finaliseOrder(userId, bookService.getTotal(userId));
        } catch (BookException ex) {
            return new ResponseEntity<>(new BookServiceResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public ResponseEntity<BookServiceResponse> getTotal(@RequestParam Integer userId){
        Integer total;
        try {
            total = bookService.getTotal(userId);
        } catch (BookException ex) {
            return new ResponseEntity<>(new BookServiceResponse(ex.getMessage()),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BookServiceResponse(total), HttpStatus.OK);
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public ResponseEntity<List<CartBook>> getCart(@RequestParam Integer userId){
        List<CartBook> cart = new ArrayList<>();
        try {
            cart = bookService.getCart(userId);
        } catch (BookException ex) {
            return new ResponseEntity<>(cart, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(cart,HttpStatus.OK);
    }

    @RequestMapping(value = "/borrowed", method = RequestMethod.GET)
    public ResponseEntity<List<Book>> getBorrowedBooks(@RequestParam Integer userId){
        List<Book> cart = new ArrayList<>();
        try {
            cart = bookService.getBorrowedBooks(userId);
        } catch (BookException ex) {
            return new ResponseEntity<>(cart, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(cart,HttpStatus.OK);
    }

    @RequestMapping(value = "/bought", method = RequestMethod.GET)
    public ResponseEntity<List<Book>> getBoughtBooks(@RequestParam Integer userId){
        List<Book> cart = new ArrayList<>();
        try {
            cart = bookService.getBoughtBooks(userId);
        } catch (BookException ex) {
            return new ResponseEntity<>(cart, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(cart,HttpStatus.OK);
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public ResponseEntity<BookServiceResponse> resetCart(@RequestParam Integer userId){
        try {bookService.resetCart(userId);
        } catch (BookException ex) {
            return new ResponseEntity<>(new BookServiceResponse("Error"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new BookServiceResponse("Ok"),HttpStatus.OK);
    }

}
