package com.javasampleapproach.angularjs.controller;

import com.javasampleapproach.angularjs.exeption.UserException;
import com.javasampleapproach.angularjs.model.UserServiceResponse;
import com.javasampleapproach.angularjs.model.User;
import com.javasampleapproach.angularjs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<UserServiceResponse> login(@RequestBody User user){
        Integer userId;
        try {
            userId = userService.loginUser(user);
        } catch (UserException ex) {
            return new ResponseEntity<>(new UserServiceResponse(ex.getMessage(), null), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new UserServiceResponse("Login ok", userId), HttpStatus.OK);
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public ResponseEntity<UserServiceResponse> register(@RequestBody User user){
        Integer userId;
        try {
            userId = userService.registerUser(user);
        } catch (UserException ex) {
            return new ResponseEntity<>(new UserServiceResponse(ex.getMessage(), null), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new UserServiceResponse("Register ok", userId), HttpStatus.CREATED);
	}

    @RequestMapping(value="/funds", method=RequestMethod.GET)
    public ResponseEntity<UserServiceResponse> getFunds(@RequestParam Integer userId){
        Integer funds = userService.getFunds(Integer.valueOf(userId));
        return new ResponseEntity<>(new UserServiceResponse(funds), HttpStatus.OK);
    }

    @RequestMapping(value="/logout", method=RequestMethod.GET)
    public ResponseEntity logout(@RequestParam Integer userId){
        userService.logoutUser(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}