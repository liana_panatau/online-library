package com.javasampleapproach.angularjs.exeption;

public class UserException extends RuntimeException {

    public UserException(String message) {
        super(message);
    }
}
